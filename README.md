# Assignment Overview #
## Programming Applications - 4CCS1PRA ##
### Pair programming exercise ###
The task is to develop a word game where a word is to be guessed, and four pictures are displayed
as a clue. As with the classic game hangman, the word is initially shown as blanks; where the number
of blanks is the length of the word. The difference is that the player is given a selection of possible
letters (of which just a few are in the word) and they must choose from these.

Awarded Mark: 100%